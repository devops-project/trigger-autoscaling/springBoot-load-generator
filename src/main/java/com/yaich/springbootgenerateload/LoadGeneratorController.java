package com.yaich.springbootgenerateload;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 
 * Author : walid.yaich@gmail.com
 * Tested on Linux mint 20 / JDK 11
 * 
 * This class is designed to generate LOAD (RAM / CPU) so you can easily test your autoscaling configuration in your HA environment.
 * Please see README.md
 * 
 * To monitor CPU and RAM of the JVM, you can run ./jconsole that you can find in your JAVA_HOME
 * 
 * References :
 * https://quarkus.io/guides/getting-started 
 * https://alvinalexander.com/blog/post/java/java-program-consume-all-memory-ram-on-computer/
 * https://gist.github.com/SriramKeerthi/0f1513a62b3b09fecaeb
 * 
 */

@RestController
@RequestMapping("/load-generator")
public class LoadGeneratorController {


    private static final Logger LOGGER = LoggerFactory.getLogger(LoadGeneratorController.class);

    /**
     * http://localhost:8080/load-generator/eat-ram/500 : will try to allocate 500 MB.
     * This method try to allocate the amount of memory passed in parameter
     * @param amount_of_ram_to_eat
     * @return String : "Out of memory" or "Memory successully allocated ...."
     */
    @GetMapping("/eat-ram/{amount_of_ram_to_eat}")
    public String eatingRAM(@PathVariable("amount_of_ram_to_eat") int amount){
        Runtime rt = Runtime.getRuntime();
        try {
            ArrayList<byte[]> arrayOfBytes = new ArrayList<>();
            int desiredMemoryAllocation = amount; 
            //Will try to eat "amount MB" of RAM, of course, GC will always try to release memory ...
            //You can stop GC : https://www.baeldung.com/jvm-epsilon-gc-garbage-collector
            int index = 0;

            while (index < desiredMemoryAllocation)
            {
              byte[] oneMegaArray = new byte[1048576];
              arrayOfBytes.add(oneMegaArray);
              index++;
            }            
        
        } catch (OutOfMemoryError e) {
            LOGGER.error("No more Memory !", e);
            return "I m SpringBoot, I'm sorry :(\n Out of memory !";
        }

        LOGGER.info("Free memory after operation : {}", Runtime.getRuntime().maxMemory() / (1024.0 * 1024.0 * 1024.0));

        LocalDateTime dateTime = LocalDateTime.now();        
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

        return String.format("JVM Date time : %s <br>" +
                            "I m SpringBoot. <br>Memory allocated successfully ! <br>" +
                            "Free memory after operation : %.2f MB <br>" +
                            "Maximum memory for the JVM : %.2fGiB <br>", 
                            dateTime.format(formatter),
                            rt.freeMemory() / (1024.0 * 1024.0),
                            Runtime.getRuntime().maxMemory() / (1024.0 * 1024.0 * 1024.0));
    }


    /**
     * Explanation of this call http://localhost:8080/load-generator/cpu/70/4/10 :
     * 1) I want this application to consume 70% of CPU LOAD
     * 2) My CPU can process 4 parallel threads
     * 3) This operation should take 10 seconds
     * 
     * @param load 
     * @param nbThreads
     * @param duration
     */
    @GetMapping("/cpu/{load_to_generate}/{number_of_threads}/{duration}")
    public String loadCpu(@PathVariable("load_to_generate") double load, //for exemple : 80 percent of CPU
                            @PathVariable("number_of_threads") Integer nbThreads, //for exemple : 4 threads
                            @PathVariable("duration") Long duration){ //for exemple : 10 second
        
        for (Integer thread = 0; thread < nbThreads; thread++) {
            new BusyThread("Thread" + thread, load / 100, duration * 1000).start();
            LOGGER.info("Working ... Thread : {}" , thread);
        }

        return "I' m SpringBoot. <br>" +nbThreads + " Threads will be running for " + 
                duration + " seconds; they will try to maintain "+ load +"% CPU";
    }
}