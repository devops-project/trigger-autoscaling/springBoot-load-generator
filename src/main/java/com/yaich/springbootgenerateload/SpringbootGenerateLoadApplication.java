package com.yaich.springbootgenerateload;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootGenerateLoadApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootGenerateLoadApplication.class, args);
	}

}
