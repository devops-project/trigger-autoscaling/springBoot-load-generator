# بسم الله الرحمن الرحيم

# If you want to simply use the application to test your autoscaling configuration :

## 1. On your Local Machine (I'm using Linux mint 20, based on Ubuntu 20.04 LTS)

### 1.1. Check that your have JDK 11 installed :
```shell script
java -version
```
Output :
```
openjdk version "11.0.10" 2021-01-19
OpenJDK Runtime Environment AdoptOpenJDK (build 11.0.10+9)
OpenJDK 64-Bit Server VM AdoptOpenJDK (build 11.0.10+9, mixed mode)
```
if JDK 11 is not installed, you should install it : https://adoptopenjdk.net/

### 1.2. Compile and generate the jar (You need internet connection)
```shell script
git clone https://gitlab.com/devops-project/trigger-autoscaling/springBoot-load-generator.git
cd springBoot-load-generator/
./mvnw package
cd target/
ls 
```
If everything is OK, you must see : **springboot-generate-load-0.0.1-SNAPSHOT.jar**

### 1.3. Test this application in your local machine :

`java -Xmx1024M -jar springboot-generate-load-0.0.1-SNAPSHOT.jar`

Be careful, so important -Xmx1024M, means that the application will be able to take a maximum of ~1024 MB of your RAM, you have to change this parameter depending on your autoscaling configuration.

**On your browser :**
http://localhost:8080/load-generator/eat-ram/500 will try to allocate 500 MB from jvm (of course JVM Garbage collector will try te release memory ASAP).

The output on your browser should be something like that :
```
JVM Date time : 26-02-2021 09:09:20
I m SpringBoot.
Memory allocated successfully !
Free memory after operation : 3.84 MB
Maximum memory for the JVM : 1.00GiB
```

http://localhost:8080/load-generator/cpu/70/4/10 will try to consume 70% of CPU during 10 second (4 is the number of threads that the machine can run in parallel).

The output on your browser should be something like that :
```
I' m SpringBoot.
4 Threads will be running for 10 seconds; they will try to consume 70.0% CPU
```

## 2. On the Cloud to test your autoscaling configuration

I assume that we want to test AWS autoscaling configuration (Should be the same concept for Docker, K8S ...)
Create an AMI Image with JDK 11 installed that will run this application at startup.
any new instance running from your AMI image will have this up running, 

**Now, using your browser, you can play with CPU and RAM of your instances and watch the behavior of your infrastructure.**

# If you want to contribute the this application development  :
## Run the application in dev mode :
./mvnw spring-boot:run

## See Reference Documentation for Java / Spring development :
* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.4.3/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.4.3/maven-plugin/reference/html/#build-image)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.4.3/reference/htmlsingle/#boot-features-developing-web-applications)
* [Spring Boot DevTools](https://docs.spring.io/spring-boot/docs/2.4.3/reference/htmlsingle/#using-boot-devtools)
* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)
